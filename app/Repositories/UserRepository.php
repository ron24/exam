<?php

namespace App\Repositories;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

use App\User;

class UserRepository
{
    public function search(array $data)
    {
    	$users = User::filter($data)
        ->orderBy($data['sortBy'], $data['orderBy'])
        ->get()
        ->toArray();

        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $col = new Collection($users);
        $perPage = $data['perPage'];
        $currentPageSearchResults = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $results = new LengthAwarePaginator(array_values($currentPageSearchResults), count($col), $perPage);

        $resultData = $results->toArray();
        $excelData = $resultData['data'];

        foreach ($excelData as $key => $value) {
            unset($excelData[$key]['token']);
            unset($excelData[$key]['password']);
        }
                        
        return response()->json(compact('results', 'excelData'));
    }

    public function add(array $data)
    {
    	return User::create(
    		[
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'address' => $data['address'],
                'postcode' => $data['postcode'],
                'contact' => $data['contact'],
    			'email' => $data['email'],
    			'username' => $data['username'],
    			'password' => bcrypt($data['password']),
    			'status' => $data['status']
    		]
    	);
    }

    public function update(array $data)
    {
        User::find($data['id'])
        ->update(
            [
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'address' => $data['address'],
                'postcode' => $data['postcode'],
                'contact' => $data['contact'],
                'email' => $data['email'],
                'username' => $data['username'],
                'status' => $data['status']
            ]
        );
    }
}