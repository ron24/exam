<?php

namespace App\Services;

use App\Repositories\UserRepository;

use Validator;

class UserService
{

	private $userRepository;

	
	public function __construct()
    {
       $this->userRepository = resolve(UserRepository::class);
    }

	public function search(array $data)
	{
	
		return $this->userRepository->search($data);
	}

	public function add(array $data)
	{

		$validator = Validator::make($data, [
            'username' => 'unique:users,username',
        ]);

        $errors = $validator->errors();

        if ($errors->first('username')) {
            return response()->json(['message' => 'Username is already exists'], 400);
        }

		$this->userRepository->add($data);
		return response()->json(['message' => 'Successfully Added'], 200);
	}

	public function update(array $data)
	{
		
		$validator = Validator::make($data, [
            'username' => 'unique:users,username,'.$data['id'],
        ]);

        $errors = $validator->errors();

        if ($errors->first('username')) {
            return response()->json(['message' => 'Username is already exists'], 400);
        }

        $this->userRepository->update($data);
		return response()->json(['message' => 'Successfully Updated'], 200);
	}

}