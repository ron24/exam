<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
    *  Modules:
    *  1 = FM Schedule, 2 = FM Update, 3 = FM Jackpot, 4 = User
    *  Actions :
    *  1 = create, 2 = retrieve, 3 = update, 4 delete
    *  
    *  $res_id will be the reference ID of the action made by the user in certain module.
    */

}
