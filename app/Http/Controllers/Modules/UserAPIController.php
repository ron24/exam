<?php

namespace App\Http\Controllers\Modules;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Services\UserService;


class UserAPIController extends Controller
{

	private $userService;
    
    public function __construct()
    {
       $this->userService = resolve(UserService::class);
    }

	public function search(Request $request)
	{
		$data = $request->all();

		return $this->userService->search($data);
	}

	public function add(Request $request)
	{
		$data = $request->all();

		return $this->userService->add($data);
	}

	public function update(Request $request)
	{
		$data = $request->all();

        return $this->userService->update($data);
	}
}
