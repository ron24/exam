<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{

    public function login()
    {

    	if (!auth()->attempt(request(['username', 'password']))) {
    		
    		return back()->withErrors([
    			'message' => 'Please check your credentials and try again.'
    		]);

    	} 

    	else {

            return redirect('/users'); 
        
        }

    }

    public function logout()
    {

        Auth::logout();
        return redirect('login');
        
    }
    
}
