<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->status == 0)
            
            return $next($request);
        
        else {
            
            auth()->logout();

            return redirect('/login')->withErrors([
                'message' => ' Your account is inactive, please contact the administrators'
            ]);

        }
    }
}
