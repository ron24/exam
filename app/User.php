<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'address', 'postcode', 'contact', 'email',  'username', 'password', 'status', 'status'
    ];

    public function role()
    {
        return $this->hasOne('App\Models\Role');
    }

    public function scopeFilter($query, $filters)
    {   

        if (isset( $filters['name'] ) ) {
            $name = $filters['name'];
            $query->where('name', 'like', "$name%");
        }

        if (isset( $filters['username'] ) ) {
            $username = $filters['username'];
            $query->where('username', 'like', "$username%");
        }

        if (isset( $filters['role'] ) ) {
            $query->where('role_id', $filters['role']);
        }

        if (isset( $filters['status'] ) ) {
            $query->where('status', $filters['status']);
        }

    }

    public function getRememberTokenName()
    {
        return 'token';
    }
}
