<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title> Yondu Exam </title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('vendor/AdminLTE-2.4.3/dist/css/AdminLTE.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('vendor/ionicons/css/ionicons.min.css') }}">
	<script type="text/javascript"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">

	<div id="dw"></div>

	@include('partials.errors')
	
	<script src="{{ mix('js/app.js') }}"></script>
	<script src="{{ mix('js/dawang.js') }}"></script>
	<script type="text/javascript" src="{{ URL::asset('vendor/AdminLTE-2.4.3/dist/js/adminlte.min.js') }}"></script>
	
</body>

</html>