<div id="alert-messages">

	@if(count($errors))
		
		<div class="alert alert-danger alert-dismissible">

			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			
			<h4><i class="icon fa fa-ban"></i> Alert!</h4>

			<ul>
					
				@foreach ($errors->all() as $error)
					
					<li> {{ $error }} </li>
				
				@endforeach
						
			</ul>

		</div>
		
	@endif

	@if($flash = session('success'))

		<div class="alert alert-success alert-dismissible">

	        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

	        <h4><i class="icon fa fa-check"></i> Alert!</h4>

        	{{ $flash }}
      	
      	</div>
		
	@endif

</div>