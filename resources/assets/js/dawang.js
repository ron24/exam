import Vue from 'vue';
import Vuex from 'vuex';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import router from './routes';
import App from '../components/MainApp.vue';
import { globalState } from '../store/global';

Vue.use(VeeValidate);
Vue.use(VueRouter);
Vue.use(Vuex);

const app = new Vue({
    el: '#dw',
    store: globalState,
    router,
    render: h => h(App)
});
