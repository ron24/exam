import VueRouter from 'vue-router';

export default new VueRouter({
	mode: 'history',
	linkActiveClass: 'active',
	routes: [

		{
			path: '/login',
			component: require('../components/authentication/Login.vue')
		},

		{
			path: '/home',
			component: require('../components/Modules/user/main.vue')
		},

		{
			path: '/register',
			component: require('../components/Modules/register.vue')
		},

		{
			path: '/accounts',
			component: require('../components/Modules/ManageUsers.vue')
		},

		{
			path: '/users',
			component: require('../components/Modules/user/main.vue')
		},

	]
});