import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const globalState = new Vuex.Store({

  state: {
  	csrf: document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
  	ids: '',
  	userModel: {}
  },

  mutations: {
  	storeUserList(state, user) {
  		state.userModel = user
  	}
  }

})
