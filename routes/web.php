<?php

Route::resource('api/dashboard', 'ApiController');

Route::middleware(['guest'])->group(function () {

	Route::get('/login', function () {
	    return view('master');
	})->name('login');
	
	Route::post('/login', 'AuthController@login');

});

Route::group(['prefix' => 'admin/api', 'middleware' => 'auth'],	function () {

	Route::get('accounts', 'Modules\UserAPIController@show');

	Route::post('accounts/store', 'Modules\UserAPIController@store');
	
	Route::post('accounts/update', 'Modules\UserAPIController@update');

	Route::post('accounts/change_password', 'Modules\UserAPIController@change_password');

	Route::get('search-user', 'Modules\UserAPIController@search');
	Route::post('add-user', 'Modules\UserAPIController@add');
	Route::put('update-user', 'Modules\UserAPIController@update');

});

Route::middleware(['auth', 'auth.active'])->group(function () {

	Route::post('/logout', 'AuthController@logout');
	
	Route::get('/{vue_capture?}', function () {
	   return view('master');
	})->where('vue_capture', '[\/\w\.-]*');

});