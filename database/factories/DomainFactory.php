<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Domain::class, function (Faker $faker) {
    return [
        //
        'id' => null,
        'domain_name' => str_random(20),
        'username' => $faker->name,
        'expiration_date' => Carbon::now()->addWeek(),
        'status' => true,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now(),
    ];
});
