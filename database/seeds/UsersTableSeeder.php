<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'address' => 'Taguig',
            'postcode' => '1637',
            'contact' => '09237755',
            'email' => 'admin@gmail.com',
            'username' => 'admin',
            'password' => bcrypt('password'),
            'status' => 0,
            'token' => ''
        ]);

        DB::table('users')->insert([
            'firstname' => 'ronald',
            'lastname' => 'duque',
            'address' => 'Taguig',
            'postcode' => '1637',
            'contact' => '09237755',
            'email' => 'ronald@gmail.com',
            'username' => 'ronald',
            'password' => bcrypt('password'),
            'status' => 0,
            'token' => ''
        ]);

    
    }

    
}
